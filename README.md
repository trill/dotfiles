# dotfiles

My i3 config.

## Screenshots

![Desktop](screenshot/desktop.png?raw=true)

![Application](screenshot/qtcreator.png?raw=true)

![Screen lock](screenshot/screen_locked.png?raw=true)

![Desktop 2](screenshot/desktop2.png?raw=true)

![Status bar](screenshot/statusbar.png?raw=true)

![Neovim](screenshot/nvim.png?raw=true)

## Dependencies

* i3wm
* i3wsr
* i3blocks (Status bar)
* Rofi
* i3lock (Lock screen)
* ImageMagick (Screenshot)
* (Conky (Desktop clock))
* nitrogen (Wallpaper)
* xrandr (Monitor switcher)
* qt5ct https://archlinux.org/packages/community/x86_64/qt5ct/
* light (backlight controller, for devices that support it) https://github.com/haikarainen/light, https://archlinux.org/packages/community/x86_64/light/
* dunst (notify daemon)
* picom (compositor)
* sxhkd (as systemd user service)
* Source Code Pro font https://archlinux.org/packages/extra/any/adobe-source-code-pro-fonts/
* ttf-font-awesome https://archlinux.org/packages/community/any/ttf-font-awesome/
* https://codeberg.org/trill/trss for the RSS icon
* Redshift https://wiki.archlinux.org/title/Redshift
* mpd/mpc for the music block
* https://codeberg.org/trill/sadav To show a month calendar with events

and more, zsh, fzf...

## Defaults

* Terminal = `$TERMINAL` or Alacritty (`~/.xprofile`)
* Browser = `$BROWSER` or Firefox (`~/.xprofile`)
* Keyboard layout = at (`~/.local/bin/i3block/keyboard`)

## Shortcuts

Mod=Super

* Mod+0 Shutdown menu
* Mod+D Application launcher (rofi)
* Mod+L Lock screen (i3lock)
* PrtScn Screenshot all to clipboard
* Alt+PrtScn Screenshot active window to clipboard
* Mod+M Monitor switcher
* Mod+Y Window switcher
* Mod+B Launch browser
* Mod+P+{a,l,t,p,s,c} Do some MPD stuff
* Ctrl+Alt+Backspace to kill Xorg

The rest is i3 default.

### Key chords

sxhkd is used to support key chords (configured in `~/.config/sxhkd/sxhkdrc`). To enable it:
```
$ systemctl --user enable sxhkd.service
```

Alternatively in `~/.config/i3/config`:
```
exec_always --no-startup-id systemctl --user restart sxhkd.service
```

## Volume control when using PipeWire

The block shows the selected sink and the volume.

* Left click on it does nothing
* Middle click: toggle mute
* Right click: select sink
* Mouse wheel up: increase volume
* Mouse wheel down: decrease volume

## mpd control

There is a block to control mpd using mpc.

* Left click: Plays previous song in queue
* Middle click: Toggle pause/play
* Right click: Play next song in queue
* Mouse wheel up: Seek +10 sec
* Mouse wheel down: Seek -10 sec
