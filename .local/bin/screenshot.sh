#!/bin/bash

SCREEN_DIR=~/Pictures
# SCREEN_PROMPT=1

window='root'

case $1 in
  root)
    window='root';;
  active)
    window=`xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)" | cut -d' ' -f5`;;
esac

[ ! -z "$SCREEN_PROMPT" ] && \
  name=`i3-input -P 'screen-name: ' | sed -n '/command = /s/command = //p'`

if [ -z "$name" ];then
  if [ $window == "root" ];then
    name='root'
  else
    name=`xprop -id $window | sed -n '/WM_CLASS/s/.* = "\([^\"]*\)".*/\1\n/p'`
    [ -z "$name" ] && name='window'
  fi
fi

filename="$name-`date +%Y-%m-%d_%H-%M-%S`.png"
echo $SCREEN_DIR/$filename

res=$(import -border -window $window "$SCREEN_DIR/$filename" 2>&1)

status=$?

if [ $status -eq 0 ];then
    # ln -sf "$filename" $SCREEN_DIR/last
    xclip -selection clip -t "image/png" "$SCREEN_DIR/$filename"

    rm -f "$SCREEN_DIR/$filename"
else
    printf "Failed to create screenshot\n\n%s\n" "$res"
fi
exit
