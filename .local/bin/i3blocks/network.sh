#!/bin/bash

# https://codeberg.org/dvr/dotfiles/src/branch/master/.local/bin/statusbar/sb-internet

# Show wifi  and percent strength or 📡 if none.
# Show 🌐 if connected to ethernet
# Show 🔒 if a vpn connection is active
# Show 🚫 when not connected to anything

case $BLOCK_BUTTON in
	1) nm-connection-editor; pkill -RTMIN+11 i3blocks ;;
esac

if grep -xq 'up' /sys/class/net/w*/operstate 2>/dev/null ; then
	wifiicon="$(awk '/^\s*w/ { print " ", int($3 * 100 / 70) "% " }' /proc/net/wireless)"
elif grep -xq 'down' /sys/class/net/w*/operstate 2>/dev/null ; then
	grep -xq '0x1003' /sys/class/net/w*/flags && wifiicon="📡 " || wifiicon=""
fi
ethicon="$(sed "s/down//;s/up/🌐/" /sys/class/net/e*/operstate 2>/dev/null)"
vpnicon="$(sed "s/.*/🔒/" /sys/class/net/tun*/operstate 2>/dev/null)"

printf -v out "%s%s%s" "$wifiicon" "$ethicon" "$vpnicon"

if [[ -z "$out" ]]; then
    echo "🚫"
    echo "🚫"
    echo "#ff0000"
else
    echo "$out"
    echo "$out"
    echo "#018fef"
fi
