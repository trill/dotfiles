// Use userChrome.css
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
// Show Density: Compact
user_pref("browser.compactmode.show", true);
// Don't show "View saved Logins"
user_pref("signon.showAutoCompleteFooter", false);
// Don't use the URL bar for searching
user_pref("keyword.enabled", false);
// to hide the extensions button (maybe obsolete because we hide it now with CSS)
user_pref("extensions.unifiedExtensions.enabled", false);

// Search
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.urlbar.suggest.searches", false);

// Telemetry
user_pref("app.normandy.api_url", "");
user_pref("app.normandy.enabled", false);
user_pref("app.normandy.first_run", false);
user_pref("app.normandy.user_id", "");
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("app.update.auto", false); // Use system updater
user_pref("beacon.enabled", false);
user_pref("breakpad.reportURL", "");
// Ads
user_pref("dom.private-attribution.submission.enabled", false);

// Misc
user_pref("findbar.highlightAll", true);
// Backspace -> Back
user_pref("browser.backspace_action", 0);

// Enable media key
user_pref("media.hardwaremediakeys.enabled", true);
user_pref("dom.media.mediasession.enabled", true);

user_pref("browser.tabs.closeWindowWithLastTab", false);

