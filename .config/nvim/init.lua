local g   = vim.g
local o   = vim.o
local opt = vim.opt
local A   = vim.api

o.termguicolors = true

-- Do not save when switching buffers
-- o.hidden = true

-- Decrease update time
o.timeoutlen = 500
o.updatetime = 200

-- Number of screen lines to keep above and below the cursor
o.scrolloff = 8

-- Better editor UI
o.number = true
o.numberwidth = 2
o.relativenumber = true
o.signcolumn = "yes"
o.cursorline = true

-- Better editing experience
o.expandtab = true
o.smarttab = true
o.cindent = true
o.autoindent = true
o.wrap = true
o.textwidth = 300
o.tabstop = 4
o.shiftwidth = 4
o.softtabstop = -1 -- If negative, shiftwidth value is used
o.list = true
o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂"
-- o.formatoptions = "qrn1"

-- Makes neovim and host OS clipboard play nicely with each other
o.clipboard = "unnamedplus"

-- Case insensitive searching UNLESS /C or capital in search
o.ignorecase = true
o.smartcase = true

-- Undo and backup options
o.backup = false
o.writebackup = false
o.undofile = true
o.swapfile = false
-- o.backupdir = "/tmp/"
-- o.directory = "/tmp/"
-- o.undodir = "/tmp/"

-- Remember 50 items in commandline history
o.history = 50

-- Better buffer splitting
o.splitright = true
o.splitbelow = true

-- Folding
o.foldmethod = "expr"
o.foldexpr="nvim_treesitter#foldexpr()"
o.foldlevelstart = 99

opt.mouse = "a"
opt.completeopt = "menu,menuone,noinsert,"
-- opt.colorcolumn = "80"

-- Map <leader> to space
g.mapleader = " "
g.maplocalleader = " "

g.skipview_files = { "BUG_MESSAGE_EDITMSG" }

-- Highlight the region on yank
A.nvim_create_autocmd("TextYankPost", {
    group = num_au,
    callback = function()
        vim.highlight.on_yank({ higroup = "Visual", timeout = 200 })
    end,
})

-- Dis-/enable search highlight when entering/leaving insert mode
A.nvim_create_autocmd("InsertEnter", {
    command = "set nohlsearch"
})
A.nvim_create_autocmd("InsertLeave", {
    command = "set hlsearch"
})
A.nvim_create_autocmd("FileType", {
    -- Generally we want to visually wrap lines, but not for all file types.
    pattern = "cpp,c,make,lua,python,ruby,javascript",
    command = "set nowrap"
})

-- Disable conceal
vim.cmd [[
"let g:vim_json_conceal=0
"let g:markdown_syntax_conceal=0
let g:indentLine_enabled = 0
]]

require("sa.neovide")
require("sa.keys")
require("sa.plugins")
require("sa.lsp")

-- VimTeX, Zathura
g.vimtex_view_method = 'zathura'
g.latex_view_general_viewer = 'zathura'
g.vimtex_compiler_progname = 'nvr'

-- vim: tabstop=4 shiftwidth=4 softtabstop=4 expandtab
