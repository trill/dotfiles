-- Loose integration of the ninja build tool
--
-- Requires https://github.com/nvim-lua/plenary.nvim
--
-- :Ninja [<target>] just runs ninja in a certain directory
-- :NinjaDir <dir> is used to set this directory

-- Compiler used for parsing the output
local compiler = vim.g.ninja_compiler or "gcc"
-- Build directory candidates
local build_dirs = vim.g.ninja_build_dirs or {
    "build",
    "build-Release",
    "build-Debug",
    "Release",
    "Debug",
    "release",
    "debug",
}
local ninja_dir = vim.g.ninja_dir or ""
local autosave = vim.g.ninja_autosave or true

local last_target = ""

local function ninjadir()
    local cwd = vim.fn.getcwd()
    if ninja_dir ~= "" then
        return cwd .. "/" .. ninja_dir
    end

    for _, dir in pairs(build_dirs) do
        local d = cwd .. "/" .. dir
        if vim.fn.isdirectory(d) ~= 0 then
            return d
        end
    end
    -- If nothing else return current directory
    return cwd
end

local function get_target(opts)
    if opts.args == "!!" then
        return last_target
    end
    if opts.args ~= nil and opts.args ~= "" then
        return opts.args
    end
    if opts.args == nil then
        return ""
    end
    return opts.args
end

local function run(opts)
    local ndir = ninjadir();

    if autosave then
        -- Save all first
        vim.cmd.wa()
    end
    vim.opt_local.makeprg = "ninja"
    vim.cmd.compiler({ compiler })
    local make_args = { "-C", ndir }
    local target = get_target(opts)
    local starget = "(empty)"
    if target ~= "" then
        last_target = target
        starget = target
        table.insert(make_args, target)
    end
    print("Ninja: " .. starget .. " in " .. ndir)
    vim.cmd.make(make_args)
end

local function dir(opts)
    local args = opts.args or ""
    if args ~= "" then
        ninja_dir = args:gsub("/$", "")
    end
    return ninjadir()
end

local function check_dir(dirname)
    return vim.fn.empty(vim.fn.glob(dirname .. "/*.ninja")) == 0
end

local function scandir()
    local scan = require('plenary.scandir')
    local t = {}
    local i = 0
    if check_dir(vim.fn.getcwd()) then
        i = i + 1
        t[i] = "."
    end
    scan.scan_dir(vim.fn.getcwd(), {
        hidden = false,
        depth = 1,
        only_dirs = true,
        silent = true,
        on_insert = function(name)
            if vim.fn.isdirectory(name) and check_dir(name) then
                i = i + 1
                t[i] = vim.fs.basename(name)
            end
        end,
    })
    return t
end

local function targets()
    local cmd = "ninja -C " .. ninjadir() .. " -t targets"
    local i, t, popen = 0, {}, io.popen
    local pfile = popen(cmd)
    if pfile == nil then
        return { "" }
    end
    for target in pfile:lines() do
        if (not string.find(target, "/")) then
            i = i + 1
            t[i] = target:gsub(":.*$", "")
        end
    end
    if (last_target ~= "") then
        i = i + 1
        t[i] = "!!"
    end
    pfile:close()
    return t
end

vim.api.nvim_create_user_command(
    'Ninja',
    function(opts)
        run(opts)
    end,
    {
        nargs = '?',
        complete = targets
    }
)

vim.api.nvim_create_user_command(
    'NinjaDir',
    function(opts)
        print("NinjaDir: " .. dir(opts))
    end,
    {
        nargs = '?',
        complete = scandir
    }
)
