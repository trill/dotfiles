function ExtendedHome()
    let column = col('.')
    normal! ^
    if column == col('.')
        normal! 0
    endif
endfunction
nnoremap <Home> <cmd>call ExtendedHome()<CR>
inoremap <Home> <cmd>call ExtendedHome()<CR>
