" Wipe all old buffers with :Bwipeout
func! s:Delete_buffers()
    let l:buffers = filter(getbufinfo(), {_, v -> v.hidden})
    if !empty(l:buffers)
        execute 'bwipeout' join(map(l:buffers, {_, v -> v.bufnr}))
    endif
endfunc
command! -bar Bwipeout call s:Delete_buffers()
