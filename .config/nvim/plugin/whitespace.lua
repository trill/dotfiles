vim.api.nvim_create_user_command(
    'TrimSpace',
    function()
        vim.cmd("%s/\\s\\+$//e")
    end,
    {
        nargs = 0,
    }
)
