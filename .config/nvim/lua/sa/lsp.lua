-------------------------------------------------
-- LSP
-------------------------------------------------

local map_key = require("sa.map_key")

-- Signature help with LSP
local lsp_sig_cfg = {
    debug = false, -- set to true to enable debug logging
    log_path = vim.fn.stdpath("cache") .. "/lsp_signature.log", -- log dir when debug is on
    -- default is  ~/.cache/nvim/lsp_signature.log
    verbose = false, -- show debug line number

    bind = true, -- This is mandatory, otherwise border config won't get registered.
    -- If you want to hook lspsaga or other signature handler, pls set to false
    doc_lines = 10, -- will show two lines of comment/doc(if there are more than two lines in doc, will be truncated);
    -- set to 0 if you DO NOT want any API comments be shown
    -- This setting only take effect in insert mode, it does not affect signature help in normal
    -- mode, 10 by default

    max_height = 12, -- max height of signature floating_window
    max_width = 100, -- max_width of signature floating_window
    wrap = true, -- allow doc/signature text wrap inside floating_window, useful if your lsp return doc/sig is too long

    floating_window = true, -- show hint in a floating window, set to false for virtual text only mode

    floating_window_above_cur_line = true, -- try to place the floating above the current line when possible Note:
    -- will set to true when fully tested, set to false will use whichever side has more space
    -- this setting will be helpful if you do not want the PUM and floating win overlap

    floating_window_off_x = 1, -- adjust float windows x position.
    floating_window_off_y = 0, -- adjust float windows y position. e.g -2 move window up 2 lines; 2 move down 2 lines

    close_timeout = 4000, -- close floating window after ms when last parameter is entered
    fix_pos = false,  -- set to true, the floating window will not auto-close until finish all parameters
    hint_enable = true, -- virtual hint enable
    hint_prefix = "➥ ",  -- Prefix for parameter hint
    hint_scheme = "String",
    hi_parameter = "LspSignatureActiveParameter", -- how your parameter will be highlight
    handler_opts = {
        border = "rounded"   -- double, rounded, single, shadow, none
    },

    always_trigger = false, -- sometime show signature on new line or in middle of parameter can be confusing, set it to false for #58

    auto_close_after = nil, -- autoclose signature float win after x sec, disabled if nil.
    extra_trigger_chars = {}, -- Array of extra characters that will trigger signature completion, e.g., {"(", ","}
    zindex = 200, -- by default it will be on top of all floating windows, set to <= 50 send it to bottom

    padding = " ", -- character to pad on left and right of signature can be " ", or "|"  etc

    transparency = nil, -- disabled by default, allow floating win transparent value 1~100
    shadow_blend = 36, -- if you using shadow as border use this set the opacity
    shadow_guibg = "Black", -- if you using shadow as border use this set the color e.g. "Green" or "#121315"
    timer_interval = 200, -- default timer check interval set to lower value if you want to reduce latency
    toggle_key = nil, -- toggle signature on and off in insert mode,  e.g. toggle_key = "<M-x>"

    select_signature_key = nil, -- cycle to next signature, e.g. "<M-n>" function overloading
    move_cursor_key = nil, -- imap, use nvim_set_current_win to move cursor between current win and floating
}

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    -- Enable completion triggered by <c-x><c-o>
    -- Using https://github.com/hrsh7th/nvim-cmp now
--    vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

    local function map_buf(m, k, v, d)
        vim.keymap.set(m, k, v, { noremap = true, silent = true, buffer = bufnr, desc = d })
    end
    local lsp = vim.lsp.buf
    local A   = vim.api

    -- Mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    map_buf("n", "<F2>", lsp.declaration, "Goto declaration")
    map_buf("n", "gd", lsp.definition, "Goto definition")
    map_buf("n", "gt", lsp.type_definition, "Goto type definition")
    map_buf("n", "<leader>li", lsp.implementation, "Goto implementation")
    map_buf("n", "K", lsp.hover, "Hover")
    map_buf("n", "<C-k>", lsp.signature_help, "Signature help")
    map_buf("n", "<leader>wa", lsp.add_workspace_folder, "Add workspace folder")
    map_buf("n", "<leader>wr", lsp.remove_workspace_folder, "Remove workspace folder")
    map_buf("n", "<leader>wl", function()
        print(vim.inspect(lsp.list_workspace_folders()))
    end, "List workspace folders")
    map_buf("n", "<leader>lr", lsp.rename, "Rename")
    map_buf("n", "<leader>la", lsp.code_action, "Code action")
    map_buf("n", "<leader>lu", lsp.references, "List all References")
    map_buf("n", "<leader>lf", lsp.format, "LSP Format")

    if client.name == "clangd" then
        -- Works only with clangd LSP attached
        map_key("n", "<F4>", "<CMD>ClangdSwitchSourceHeader<CR>", "Switch source/header file")
        map_key("n", "<leader>hv", "<CMD>vsplit | ClangdSwitchSourceHeader<CR>", "Open source/header in vertical split")
        map_key("n", "<leader>hh", "<CMD>split | ClangdSwitchSourceHeader<CR>", "Open source/header in split")
    end

    -- Automatic highlighting references with LSP
    -- https://sbulav.github.io/til/til-neovim-highlight-references/
    if client.server_capabilities.documentHighlightProvider then
        A.nvim_create_augroup("lsp_document_highlight", { clear = true })
        A.nvim_clear_autocmds { buffer = bufnr, group = "lsp_document_highlight" }
        A.nvim_create_autocmd("CursorHold", {
            callback = vim.lsp.buf.document_highlight,
            buffer = bufnr,
            group = "lsp_document_highlight",
            desc = "Document Highlight",
        })
        A.nvim_create_autocmd("CursorMoved", {
            callback = vim.lsp.buf.clear_references,
            buffer = bufnr,
            group = "lsp_document_highlight",
            desc = "Clear All the References",
        })
    end

    -- https://github.com/SmiteshP/nvim-navic
    if client.server_capabilities.documentSymbolProvider then
        require("nvim-navic").attach(client, bufnr)
    end
    -- https://github.com/simrat39/inlay-hints.nvim
    require("lsp-inlayhints").on_attach(client, bufnr)
    require("lsp_signature").on_attach(lsp_sig_cfg, bufnr)
end

local lspconfig_found, lspconfig = pcall(require, "lspconfig")
if lspconfig_found then
    local function setup_lsp(name)
        local cap_success, cmp = pcall(require, 'cmp_nvim_lsp')
        if not cap_success then
            return
        end
        -- Suppress warning if not installed
        local success, _ = pcall(require, "lspconfig.configs." .. name)
        if success then
            local lsp = lspconfig[name]
            if name == "efm" then
                lsp.setup {
                    on_attach = on_attach,
                    filetypes = { "sh", "zsh", "make", "yaml", "json", "markdown" },
                    single_file_support = true,
                    capabilities = cmp.default_capabilities(),
                }
            elseif name == "clangd" then
                lsp.setup {
                    cmd = {
                        "clangd",
                        "--clang-tidy",
                        "--cross-file-rename",
                        "--all-scopes-completion",
                        "--completion-style=detailed",
                        "--header-insertion=iwyu",
                        "--suggest-missing-includes",
                        "--background-index",
                        -- https://github.com/clangd/clangd/issues/922
                        "--function-arg-placeholders=0"
                    },
                    on_attach = on_attach,
                    capabilities = cmp.default_capabilities(),
                    init_options = {
                        clangdFileStatus = true,
                        usePlaceholders = true,
                        completeUnimported = true
                    }
                }
            else
                lsp.setup {
                    on_attach = on_attach,
                    capabilities = cmp.default_capabilities(),
                }
            end
        end
    end

    setup_lsp("clangd")        -- C/C++
    setup_lsp("neocmake")      -- CMake
    setup_lsp("rust_analyzer") -- Rust
    setup_lsp("texlab")        -- LaTeX
    --setup_lsp("ltex")          -- LanguageTool
    setup_lsp("pylsp")         -- Python
    setup_lsp("lua_ls")        -- Lua
    setup_lsp("quick_lint_js") -- JavaScript
    setup_lsp("solargraph")    -- Ruby
    setup_lsp("gopls")         -- Go
    setup_lsp("satl")          -- sa::tl
    -- https://github.com/mattn/efm-langserver
    -- See ~/.config/efm-langserver/config.yaml
    setup_lsp("efm")
end

