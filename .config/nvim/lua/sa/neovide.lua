if vim.g.neovide then
    vim.o.guifont = "Source Code Pro:h10"
--    g.neovide_cursor_animation_length = 0
    vim.g.neovide_scroll_animation_length = 0
    vim.g.neovide_remember_window_size = true
    vim.g.neovide_remember_window_position = true

    -- Let's use Ctrl++ and Ctrl+- for zoom in/out because this is what usually works in terminals as well
    local map_key = require("sa.map_key")
    map_key("n", "<C-+>", function()
        vim.g.neovide_scale_factor = vim.g.neovide_scale_factor + 0.1
    end, "Zoom in")

    map_key("n", "<C-->", function()
        if vim.g.neovide_scale_factor > 0.1 then
            vim.g.neovide_scale_factor = vim.g.neovide_scale_factor - 0.1
        end
    end, "Zoom out")
    map_key("i", "<CS-v>", "<C-R>+")
    map_key("i", "<S-Insert>", "<C-R>+")
end
