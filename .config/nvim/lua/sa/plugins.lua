-- https://www.youtube.com/watch?v=aqlxqpHs-aQ
--
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out, "WarningMsg" },
            { "\nPress any key to exit..." },
        }, true, {})
        vim.fn.getchar()
        os.exit(1)
    end
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    "nvim-tree/nvim-web-devicons",
    "nvim-lua/plenary.nvim",

    -- Telescope and related plugins --
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {"nvim-lua/plenary.nvim"}
    },
    {
        "nvim-telescope/telescope-file-browser.nvim",
        config = function()
            require("telescope").setup( {
                extensions = {
                    file_browser = {
                        -- disables netrw and use telescope-file-browser in its place
                        hijack_netrw = true,
                        mappings = {
                            ["i"] = {
                                -- your custom insert mode mappings
                            },
                            ["n"] = {
                                -- your custom normal mode mappings
                            },
                        },
                    },
                    fzf = {
                        fuzzy = false,                   -- false will only do exact matching
                        override_generic_sorter = true,  -- override the generic sorter
                        override_file_sorter = true,     -- override the file sorter
                        case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
                        -- the default case_mode is "smart_case"
                    },
                },
            } )
        end
    },
    {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'
    },

    -- Treesitter --
    {
        "nvim-treesitter/nvim-treesitter",
        config = function()
            require("nvim-treesitter.configs").setup {
                ensure_installed = { "lua" },
                -- If TS highlights are not enabled at all, or disabled via `disable` prop,
                -- highlighting will fallback to default Vim syntax highlighting
                highlight = {
                    enable = true,
                    disable = {
                        "latex", -- VimTeX highlighting is better IMHO
                        "bash",
                        "json",
                    },
                    additional_vim_regex_highlighting = false,
                },
                indent = {
                    enable = false  -- Not there yet
                },
            }
        end
    },

    -- Completion --
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/cmp-vsnip",
    "hrsh7th/vim-vsnip",
    {
        "hrsh7th/nvim-cmp",
        config = function()
            local kind_icons = {
                Text = "",
                Method = "",
                Function = "",
                Constructor = "",
                Field = "",
                Variable = "",
                Class = "ﴯ",
                Interface = "",
                Module = "",
                Property = "ﰠ",
                Unit = "",
                Value = "",
                Enum = "",
                Keyword = "",
                Snippet = "",
                Color = "",
                File = "",
                Reference = "",
                Folder = "",
                EnumMember = "",
                Constant = "",
                Struct = "",
                Event = "",
                Operator = "",
                TypeParameter = ""
            }
            -- https://vonheikemen.github.io/devlog/tools/setup-nvim-lspconfig-plus-nvim-cmp/
            local cmp = require("cmp")
            cmp.setup({
                snippet = {
                    expand = function(args)
                        vim.fn["vsnip#anonymous"](args.body)
                    end
                },
                window = {
                    completion = cmp.config.window.bordered(),
                    documentation = cmp.config.window.bordered(),
                },
                mapping = cmp.mapping.preset.insert({
                    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
                    ['<C-f>'] = cmp.mapping.scroll_docs(4),
                    ['<C-Space>'] = cmp.mapping.complete(),
                    ['<C-c>'] = cmp.mapping.abort(),
                    -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
                    ['<CR>'] = cmp.mapping.confirm({
                        behavior = cmp.ConfirmBehavior.Insert,
                        select = true,
                    }),
                }),
                sources = cmp.config.sources({
                    { name = 'nvim_lsp', priority = 9, max_item_count = 50, keyword_length = 2 },
                    { name = 'vsnip',    priority = 5, max_item_count =  5, keyword_length = 2 },
                    { name = 'buffer',   priority = 2, max_item_count =  5, keyword_length = 3 },
--                    { name = "path",     priority = 1 },
                }),
                completion = {
                    autocomplete = false,
                    completeopt = 'menu,menuone,noinsert',
                },
                sorting = {
--                    priority_weight = 1.0,
                    comparators = {
                        cmp.config.compare.score,
                        cmp.config.compare.offset,
--                        cmp.config.compare.scope,
                        cmp.config.compare.exact,
                        -- cmp.config.compare.kind,
                        -- cmp.config.compare.sort_text,
                        -- cmp.config.compare.length,
                        -- cmp.config.compare.order,
                        --
                        -- cmp.config.compare.locality,
                        -- cmp.config.compare.recently_used,
                    }
                },
                formatting = {
                    format = function(entry, vim_item)
                        vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)
                        vim_item.menu = ({
                            buffer = "[Buffer]",
                            nvim_lsp = "[LSP]",
                            vsnip = "[vsnip]",
                            nvim_lua = "[Lua]",
                            latex_symbols = "[LaTeX]",
                        })[entry.source.name]
                        return vim_item
                    end
                },
            })
        end
    },

    { "lewis6991/gitsigns.nvim", opts = {} },

    -- Productivity --
    {
        'rmagatti/auto-session',
        opts = {
            log_level = "error",
            auto_save_enabled = true,
            auto_session_suppress_dirs = { "~/", "~/Downloads", "/"},
        }
    },
    { "windwp/nvim-autopairs",   opts = {} },
    "Yggdroot/indentLine",
    { "folke/which-key.nvim",    opts = {} },

    -- A better status line --
    {
        "nvim-lualine/lualine.nvim",
        dependencies = { "nvim-web-devicons", opt = true },
        opts = {}
    },
    {
        "SmiteshP/nvim-navic",
        dependencies = "neovim/nvim-lspconfig",
        opts = {}
    },
    {
        "utilyre/barbecue.nvim",
        dependencies = {
            "SmiteshP/nvim-navic",
            "nvim-tree/nvim-web-devicons", -- optional dependency
        },
        opts = {
            exclude_filetypes = { "toggleterm", "gitcommit", "gitrebase" }
        },
    },

    -- Configurations for Nvim LSP
    { "williamboman/mason.nvim", opts = {} },
    { "williamboman/mason-lspconfig.nvim", opts = {} },
    "neovim/nvim-lspconfig",
    "ray-x/lsp_signature.nvim",
    {
        "lvimuser/lsp-inlayhints.nvim",
        opts = {
            inlay_hints = {
                parameter_hints = {
                    show = false
                }
            }
        }
    },

    -- LaTeX
    "lervag/vimtex",

    -- Syntax Highlighting and Colors --
    "navarasu/onedark.nvim",
    "ap/vim-css-color",

    -- Other stuff --
    "ryanoasis/vim-devicons",
    "sbdchd/neoformat",
    "terryma/vim-multiple-cursors",
    -- Toggle maximizing views with <F3>
    "szw/vim-maximizer",
})

local theme_success, theme = pcall(require, "onedark")
if theme_success then
    theme.setup({
        style = "darker",
        ending_tildes = true,
    })
    theme.load()
end

local function load_telescope_ext(name)
    local success, tele = pcall(require, "telescope")
    if success then
        pcall(function()
            tele.load_extension(name)
        end)
    end
end

load_telescope_ext("file_browser")
load_telescope_ext('fzf')

-- vim: tabstop=4 shiftwidth=4 softtabstop=4 expandtab
