return function(m, k, v, d)
   vim.keymap.set(m, k, v, { noremap = true, silent = true, desc = d })
end

