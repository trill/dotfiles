-------------------------------------------------
-- KEYBINDINGS
-------------------------------------------------
local map_key = require("sa.map_key")

map_key("n", "\\", "<CMD>nohlsearch<CR>", "Unhighlight search results until n or N is pressed")

-- Buffer
map_key("n", "<leader>bd", "<CMD>bdelete<CR>", "Delete buffer")
map_key("n", "<leader>bw", "<CMD>Bwipeout<CR>", "Wipeout deleted buffers")
map_key("n", "<leader>bx", "<CMD>%bd|e#<CR>", "Delete all buffers but this")
map_key("n", "<C-w>c", "<CMD>Kwbd<CR>", "Close buffer preserving the layout")
-- Telescope
map_key("n", "<leader>fr", "<CMD>Telescope oldfiles<CR>", "Recent files")
map_key("n", "<leader>ff", "<CMD>Telescope find_files<CR>", "Find files")
map_key("n", "<leader>fb", "<CMD>Telescope file_browser<CR>", "File browser")
map_key("n", "<leader>fg", "<CMD>Telescope git_files<CR>", "Git files")
map_key("n", "<leader>fw", "<CMD>Telescope live_grep<CR>", "Live grep")
map_key("n", "<leader>bl", "<CMD>Telescope buffers<CR>", "List buffers")
map_key("n", "<C-s>", "<CMD>Telescope lsp_dynamic_workspace_symbols<CR>", "LSP Worksapce symbols")
map_key("n", "<leader>ls", "<CMD>Telescope lsp_document_symbols<CR>", "LSP Document symbols")
map_key("n", "<leader>gl", "<CMD>Telescope git_commits<CR>", "Git commits")
map_key("n", "<leader>gf", "<CMD>Telescope git_files<CR>", "Git files")
map_key("n", "<leader>gs", "<CMD>Telescope git_status<CR>", "Git status")
-- gitsigns
map_key("n", "<leader>gB", "<CMD>Gitsigns toggle_current_line_blame<CR>", "Toggle git blame")
map_key("n", "<leader>gb", "<CMD>Gitsigns blame_line<CR>", "Blame line")
map_key("n", "<leader>ga", "<CMD>Gitsigns stage_hunk<CR>", "Git stage hunk")
map_key("n", "<leader>gu", "<CMD>Gitsigns undo_stage_hunk<CR>", "Git undo stage hunk")
-- Neoformat
map_key("n", "<leader>ln", "<CMD>Neoformat<CR>", "Neoformat format")
-- Exit terminal mode with escape. Try <C-\> with a german keyboard layout :P
map_key("t", "<Esc>", "<C-\\><C-n>", "Exit terminal mode")
-- Ninja, see ~/.config/nvim/plugin/ninja.lua
map_key("n", "<leader>nb", "<CMD>Ninja<CR>", "Ninja build")
map_key("n", "<leader>nc", "<CMD>Ninja clean<CR>", "Ninja clean")
map_key("n", "<leader>nr", "<CMD>Ninja clean<CR><CMD>Ninja<CR>", "Ninja rebuild")
map_key("n", "<leader>nt", "<CMD>Ninja test<CR>", "Ninja test")
map_key("n", "<leader>ni", "<CMD>Ninja install<CR>", "Ninja install")
map_key("n", "<leader>nD", "<CMD>NinjaDir build-Debug<CR>", "Set Debug directory")
map_key("n", "<leader>nR", "<CMD>NinjaDir build-Release<CR>", "Set Release directory")
map_key("n", "[c", "<CMD>cp<CR>", "Previous compile error :cp")
map_key("n", "]c", "<CMD>cn<CR>", "Next compile error :cn")

-------------------------------------------------
-- LSP
-------------------------------------------------
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
map_key("n", "<leader>do", vim.diagnostic.open_float, "Open diagnostic in float")
map_key("n", "<leader>dp", vim.diagnostic.goto_prev, "Goto previous diagnostics")
map_key("n", "<leader>dn", vim.diagnostic.goto_next, "Goto next diagnostics")
map_key("n", "[e", vim.diagnostic.goto_prev, "Previous diagnostics")
map_key("n", "]e", vim.diagnostic.goto_next, "Next diagnostics")
map_key("n", "<leader>dl", vim.diagnostic.setloclist, "List diagnostics")

-- vim: tabstop=4 shiftwidth=4 softtabstop=4 expandtab
